set fenc=utf-8                                                              
set nobackup
set noswapfile
set showcmd

set number
set cursorline
set smartindent
set visualbell

set wrapscan
set hlsearch

set tabstop=4
set shiftwidth=4

colorscheme desert
syntax on

set clipboard+=unnamed
